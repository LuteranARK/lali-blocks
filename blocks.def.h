//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
    {"",        "cpu_usage",			    2,		        0},
    {"",        "mem.sh",					15,		        0},
    {"",        "bat.sh",					240,		    0},
    {"",        "vol.sh",					0,		        10},
    {"",        "wth",	                    2000,		    0},
    {"",        "date.sh",					10,		        0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "";
static unsigned int delimLen = 5;
